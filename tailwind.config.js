module.exports = {
  content: [
    "./src/pages/**/*.{html,js,jsx,tsx}",
    "./src/components/**/*.{html,js,jsx,tsx}",
    "./node_modules/@eco/stratos-*/dist/**/*.{js,ts,jsx,tsx}",
  ],
  presets: [require("@eco/stratos-runtime/src/styles/stratos-tailwind.js")],
};
