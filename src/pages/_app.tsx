import React from "react";
import Head from "next/head";

import { AppShell, NavigationStatusProvider } from "@eco/stratos-appShell";
import { Config } from "../../stratos-config";
import { ProtectedApp, UserProvider } from "@eco/stratos-auth";
import { MessengerProvider } from "@eco/stratos-messenger";
import { NotifierContextProvider } from "react-headless-notifier";

import "../styles/style.css";

function MyApp({ Component, pageProps }) {
  return (
    <UserProvider errorPath="">
      <ProtectedApp>
        <MessengerProvider>
          <NotifierContextProvider
            // All props are optional, those are the values by default
            config={{
              max: null,
              duration: 5000,
              position: "bottomRight", // You can specify a position where the notification should appears, valid positions are 'top', 'topRight', 'topLeft', 'bottomRight', 'bottomLeft', 'bottom'.
            }}
          >
            <NavigationStatusProvider
              initialStatus={{
                status: [
                  {
                    id: "nav-statuses",
                  },
                ],
              }}
            >
              <Head>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width" />
                <link
                  rel="icon"
                  href="https://static-assets.dtn.com/branding/dtn/v1/dtn-favicon-32.png"
                />
                <title>{Config.branding.name}</title>
              </Head>
              <AppShell navigation={Config.navItems} branding={Config.branding}>
                <Component {...pageProps} />
              </AppShell>
            </NavigationStatusProvider>
          </NotifierContextProvider>
        </MessengerProvider>
      </ProtectedApp>
    </UserProvider>
  );
}

// Opt out of static optimization for runtime env vars
// https://nextjs.org/docs/api-reference/next.config.js/runtime-configuration
MyApp.getInitialProps = async () => {
  return {};
};

export default MyApp;
