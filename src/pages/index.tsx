import Head from "next/head";
import React from "react";
import { PageShell, SingleColumnCentered } from "@eco/stratos-components";
import type { PageShellActionOptions } from "@eco/stratos-components/dist/page-shell/PageShell";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();

  const handleClick = (e) => {
    e.preventDefault();
    router.push("/");
  };

  // The following two methods are used in conjunction with the PageShell Component
  const action1Handler = () => {
    console.log("action1Handler Clicked");
  };

  const action2Handler = () => {
    console.log("action2Handler Clicked");
  };

  const pageHeaderActionOptions: PageShellActionOptions = {
    actionButtons: [
      {
        label: "Edit",
        handler: action1Handler,
        variant: "primary",
        icon: "pencil",
      },
      {
        label: "Delete",
        handler: action2Handler,
        variant: "positive",
        icon: "trash-can",
      },
    ],
  };

  return (
    <React.Fragment>
      <Head>
        <title>Eco - Starter Kit Homepage</title>
      </Head>

      <PageShell actionOptions={pageHeaderActionOptions}>
        <SingleColumnCentered>
          <h1 className="my-5 text-xl font-bold">Initial page goes here!</h1>
          <p className="text-base-700 ">
            Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. Cum sociis natoque penatibus et magnis dis
            parturient montes, nascetur ridiculus mus. Curabitur blandit tempus
            porttitor. Nullam id dolor id nibh ultricies vehicula ut id elit.
            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec
            sed odio dui.
          </p>
          <button
            className="mx-auto mt-4 w-28 border-2 border-solid border-primary-500"
            onClick={handleClick}
          >
            {" "}
            RELOAD
          </button>
        </SingleColumnCentered>
      </PageShell>
    </React.Fragment>
  );
}
