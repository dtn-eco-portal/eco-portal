/* eslint-disable react/display-name */
import Head from "next/head";
import React, {
  FC,
  forwardRef,
  ReactElement,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import {
  Button,
  Icon,
  PageShell,
  SingleColumnCentered,
} from "@eco/stratos-components";

import {
  Column,
  ColumnInstance,
  useTable,
  usePagination,
  useSortBy,
  useRowSelect,
  useGlobalFilter,
  useFlexLayout,
  useAsyncDebounce,
  FilterValue,
  CellProps,
  HeaderProps,
  Hooks,
} from "react-table";
import { DataTable } from "@eco/stratos-components";

export default function Datatable() {
  const columns = useMemo(
    () =>
      [
        { Header: "Name", accessor: "name" },
        { Header: "Title", accessor: "title" },
        {
          Header: "Email",
          accessor: "email",
          minWidth: 100,
          width: 220,
          maxWidth: 220,
        },
        {
          Header: "Role",
          accessor: "role",
          minWidth: 60,
          width: 70,
          maxWidth: 100,
        },
        {
          Header: "",
          accessor: "actions",
          minWidth: 50,
          width: 70,
          maxWidth: 70,
          disableSortBy: true,
        },
      ] as Column[],
    [],
  );

  const buttonActions = (
    <div className="flex flex-row">
      <span className="px-1">
        <Icon icon="pencil" size="xs" aria-hidden="true" />
      </span>
      <span className="pl-2 pr-4">
        <Icon icon="trash" size="xs" aria-hidden="true" />
      </span>
    </div>
  );

  const data = useMemo(
    () => [
      {
        name: "Lindsay Walton",
        title: "Front-end Developer",
        email: "lindsay.walton@example.com",
        role: "Member",
        actions: buttonActions,
      },
      {
        name: "Courtney Henry",
        title: "Designer",
        email: "courtney.henry@example.com",
        role: "Admin",
        actions: buttonActions,
      },
      {
        name: "Tom Cook",
        title: "Director, Product Development",
        email: "tom.cook@example.com",
        role: "Member",
        actions: buttonActions,
      },
      {
        name: "Whitney Francis",
        title: "Copywriter",
        email: "whitney.francis@example.com",
        role: "Admin",
        actions: buttonActions,
      },
      {
        name: "Leonard Krasner",
        title: "Senior Designer",
        email: "leonard.krasner@example.com",
        role: "Owner",
        actions: buttonActions,
      },
      {
        name: "Floyd Miles",
        title: "Principal Designer",
        email: "floy.dmiles@example.com",
        role: "Member",
        actions: buttonActions,
      },
      {
        name: "Emily Selman",
        title: "VP, User Experience",
        email: "emily.selman@example.com",
        role: "Member",
        actions: buttonActions,
      },
      {
        name: "Kristin Watson",
        title: "VP, Human Resources",
        email: "kristin.watson@example.com",
        role: "Admin",
        actions: buttonActions,
      },
      {
        name: "Emma Dorsey",
        title: "Senior Front-end Developer",
        email: "emma.dorsey@example.com",
        role: "Member",
        actions: buttonActions,
      },
      {
        name: "Alicia Bell",
        title: "Junior Copywriter",
        email: "alicia.bell@example.com",
        role: "Admin",
        actions: buttonActions,
      },
      {
        name: "Jenny Wilson",
        title: "Studio Artist",
        email: "jenny.wilson@example.com",
        role: "Owner",
        actions: buttonActions,
      },
      {
        name: "Anna Roberts",
        title: "Partner, Creative",
        email: "anna.roberts@example.com",
        role: "Member",
        actions: buttonActions,
      },
      {
        name: "Benjamin Russel",
        title: "Director, Print Operations",
        email: "benjamin.russel@example.com",
        role: "Member",
        actions: buttonActions,
      },
    ],
    [],
  );

  interface IIndeterminateInputProps {
    indeterminate?: boolean;
    value?: string;
  }

  const IndeterminateCheckbox = forwardRef<
    HTMLInputElement,
    IIndeterminateInputProps
  >(({ indeterminate, value, ...rest }, ref: React.Ref<HTMLInputElement>) => {
    const defaultRef = useRef(null);
    const resolvedRef = ref || defaultRef;

    useEffect(() => {
      (
        resolvedRef as React.MutableRefObject<HTMLInputElement>
      ).current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return <input type="checkbox" ref={resolvedRef} {...rest} />;
  });

  interface GlobalFilterProps {
    globalFilter: string;
    setGlobalFilter: (filterValue: FilterValue) => void;
  }

  const GlobalFilter: FC<GlobalFilterProps> = ({
    globalFilter,
    setGlobalFilter,
  }): ReactElement => {
    const [value, setValue] = useState<string>(globalFilter);
    const onChange = useAsyncDebounce((val: string) => {
      setGlobalFilter(val || undefined);
    }, 500);

    return (
      <div className="flex flex-wrap justify-between">
        <div className="relative rounded-md py-5">
          <input
            type="text"
            name="datatableSearch"
            id="datatableSearch"
            className="block w-full rounded-md border-base-300 pr-10 focus:border-base-500 focus:ring-base-500 sm:text-sm"
            value={value !== "undefined" ? value : ""}
            onChange={(e) => {
              setValue(e.target.value);
              onChange(e.target.value);
            }}
            placeholder="Search"
          />
          <div
            className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3"
            aria-labelledby="search icon"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5 text-base-400"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth="2"
            >
              <title>Search Icon</title>
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
              />
            </svg>
          </div>
        </div>
        <div className="py-5">
          <Button variant="primary">Add User</Button>
        </div>
      </div>
    );
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageOptions,
    gotoPage,
    nextPage,
    previousPage,
    state: { pageIndex, globalFilter },
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageIndex: 0,
        pageSize: 5,
        sortBy: [{ id: "name", desc: false }],
      },
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
    useRowSelect,
    useFlexLayout,
    (hooks: Hooks<any>) => {
      hooks.visibleColumns.push((columns: ColumnInstance[]) => {
        return [
          {
            id: "selection",
            width: 30,
            groupByBoundary: true,
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <div>
                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
              </div>
            ),
            Cell: ({ row }) => (
              <div>
                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
              </div>
            ),
          },
          ...columns,
        ];
      });
    },
  );

  return (
    <React.Fragment>
      <Head>
        <title>Eco - Starter Kit Homepage</title>
      </Head>

      <PageShell>
        <SingleColumnCentered>
          <div className="px-10">
            <DataTable
              layout="compact"
              pageCount={pageOptions.length}
              pageIndex={pageIndex}
              onPrevPage={() => previousPage()}
              onNextPage={() => nextPage()}
              onGotoPage={(e) => gotoPage(e)}
            >
              <div className="inline-block min-w-full py-2 align-middle">
                <GlobalFilter
                  globalFilter={String(globalFilter)}
                  setGlobalFilter={setGlobalFilter}
                />
              </div>
              <table {...getTableProps()}>
                <thead>
                  {headerGroups.map((headerGroup, hIdx) => (
                    <tr key={hIdx} {...headerGroup.getHeaderGroupProps()}>
                      {headerGroup.headers.map((column, idx) => (
                        <th
                          key={idx}
                          {...column.getHeaderProps(
                            column.getSortByToggleProps(),
                          )}
                          scope="col"
                        >
                          <span className="inline-block">
                            {column.render("Header")}
                          </span>
                          <span className="px-1">
                            {column.isSorted ? (
                              column.isSortedDesc ?? false ? (
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="inline-block h-4 w-4 align-middle"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="1"
                                  aria-labelledby="chevron down icon"
                                  style={{ width: "14px", height: "14px" }}
                                >
                                  <title>Chevron Down Icon</title>
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M19 9l-7 7-7-7"
                                  />
                                </svg>
                              ) : (
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="inline-block h-4 w-4 align-middle"
                                  fill="none"
                                  viewBox="0 0 24 24"
                                  stroke="currentColor"
                                  strokeWidth="1"
                                  aria-labelledby="chevron up icon"
                                  style={{ width: "14px", height: "14px" }}
                                >
                                  <title>Chevron Up Icon</title>
                                  <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M5 15l7-7 7 7"
                                  />
                                </svg>
                              )
                            ) : (
                              ""
                            )}
                          </span>
                        </th>
                      ))}
                    </tr>
                  ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                  {page.map((row, rIdx) => {
                    prepareRow(row);
                    return (
                      <tr key={rIdx} {...row.getRowProps()}>
                        {row.cells.map((cell, cIdx) => {
                          return (
                            <td key={cIdx} {...cell.getCellProps()}>
                              {cell.render("Cell")}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </DataTable>
          </div>
        </SingleColumnCentered>
      </PageShell>
    </React.Fragment>
  );
}
