module.exports = {
  ...require("@eco/stratos-prettier-config"),
  htmlWhitespaceSensitivity: "ignore",
};
