import type { NavigationItem } from "@eco/stratos-components";

interface StratosConfig {
  navItems: NavigationItem[];
  branding: {
    name: string;
    logo: string;
  };
  analytics: {
    key: string;
  };
  favicon: string;
}

export const Config: StratosConfig = {
  navItems: [
    {
      href: "/",
      label: "Home",
      icon: "house",
      external: false,
    },
    {
      href: "/datatable",
      label: "Datatable",
      icon: "table",
      external: false,
    },
  ],
  branding: {
    name: "Eco App Starter Kit",
    logo: "/eco.svg",
  },
  analytics: {
    key: "orsKDjssZTVO72yQYu4yLbZQHvBj1XAi",
  },
  favicon: "/favicon/dtn/",
};
