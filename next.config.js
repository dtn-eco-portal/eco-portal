/** @type {import('next').NextConfig} */

const withTM = require("next-transpile-modules")([
  "@eco/stratos-appShell",
  "@eco/stratos-components",
  "@eco/stratos-validation",
  "@eco/stratos-runtime",
  "@eco/stratos-analytics",
  "@eco/stratos-auth",
  "@eco/stratos-messenger",
  "react-syntax-highlighter",
]);

const ContentSecurityPolicy = [
  "default-src https: 'self' https://static-assets.dtn.com",
  "object-src 'none'",
  "style-src 'unsafe-inline' 'self' http: https: http://localhost:3000 https://*.dtn.com",
  "script-src https: 'self' https://static-assets.dtn.com 'unsafe-eval' https://cdn.segment.com",
  "connect-src https: data: blob: 'self' https://static-assets.dtn.com https://api.segment.io https://eco-messenger-api.prd.coreservices.zones.dtn.com",
  "img-src https: data: 'self' https://static-assets.dtn.com",
];

const PermissionsPolicy = [
  "accelerometer=()",
  "autoplay=()",
  "camera=()",
  "display-capture=()",
  "document-domain=()",
  "encrypted-media=()",
  "geolocation=()",
  "gyroscope=()",
  "magnetometer=()",
  "microphone=()",
  "midi=()",
  "payment=()",
  "picture-in-picture=()",
  "publickey-credentials-get=()",
  "screen-wake-lock=()",
  "sync-xhr=(self)",
  "usb=()",
  "web-share=()",
  "xr-spatial-tracking=()",
];

const securityHeaders = [
  {
    // Inform browsers that only HTTPS should be used.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
    key: "Strict-Transport-Security",
    value: "max-age=63072000; includeSubDomains; preload",
  },
  {
    // [DEPRECATED by Content-Security-Policy. Retained for older
    // browser support]
    // Prevent loading pages that may be compromised by Cross Site
    // Scripting (XSS) attacks. Be careful if changing the mode while
    // keeping the overall configuration enabled (see
    // "Vulnerabilities caused by XSS filtering" in the linked
    // document).
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
    key: "X-XSS-Protection",
    value: "1; mode=block",
  },
  {
    // Prevent rendering embedded content (e.g., <iframe>) from other
    // origins.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
    key: "X-Frame-Options",
    value: "SAMEORIGIN",
  },
  {
    // Prevent browsers from guessing content types when not specified.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
    key: "X-Content-Type-Options",
    value: "nosniff",
  },
  {
    // Restrict how much information the browser provides to other sites.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
    key: "Referrer-Policy",
    value: "strict-origin-when-cross-origin",
  },
  {
    // Limit 3rd party locations that content can be loaded from.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
    key: "Content-Security-Policy",
    value: ContentSecurityPolicy.join("; "),
  },
  {
    // Limit which browser features are made available. This feature
    // is EXPERIMENTAL.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy
    key: "Permissions-Policy",
    value: PermissionsPolicy.join(", "),
  },
];

const nextConfig = {
  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },
  reactStrictMode: true,
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  // Auth Config
  serverRuntimeConfig: {
    DTN_APP_DOMAIN: process.env.DTN_APP_DOMAIN,
    DTN_CLIENT_ID: process.env.DTN_CLIENT_ID,
    DTN_CLIENT_SECRET: process.env.DTN_CLIENT_SECRET,
  },
  publicRuntimeConfig: {
    DTN_PRODUCT_CODE: process.env.DTN_PRODUCT_CODE || "",
    ECO_MESSENGER_URL: process.env.ECO_MESSENGER_URL,
    ECO_MESSENGER_PUSH_URL: process.env.ECO_MESSENGER_PUSH_URL,
    ECO_MESSENGER_PUSH_DELAY: process.env.ECO_MESSENGER_PUSH_DELAY,
    ECO_MESSENGER_PULL_URL: process.env.ECO_MESSENGER_PULL_URL,
    ECO_MESSENGER_PULL_INTERVAL: process.env.ECO_MESSENGER_PULL_INTERVAL,
  },
  // Default headers
  async headers() {
    return [
      {
        // Apply secure headers to all routes by default
        source: "/:path*",
        headers: securityHeaders,
      },
    ];
  },
};

module.exports = withTM(nextConfig);
