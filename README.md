# DTN's Eco Portal App

## Getting Started

### Setting Up the Development Environment

1. Clone the
   [repository](https://gitlab.com/dtn-eco-portal/eco-portal)
   `git clone https://<your-bitbucket-username>@gitlab.com:dtn-eco-portal/eco-portal.git`
2. `cd` in to the repository `cd eco-portal`
3. Create `.env.local` file in the root directory and paste the credentials.
3. Run `npm run install` to install the npm dependencies.
4. Run `npm run build` to build each package.
5. Run `npm run dev` to start the app locally.

Thats it 🚀!